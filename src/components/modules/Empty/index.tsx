import React from "react";
import cn from "classnames";

import s from "./Empty.module.scss";

const Empty: React.FunctionComponent<{ className?: string }> = ({
  className,
}) => {
  return <div className={cn(s.empty, className)}>Không có dữ liệu</div>;
};

export default Empty;
