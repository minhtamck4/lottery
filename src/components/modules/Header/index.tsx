import React from "react";
import cn from "classnames";
import Link from "next/link";
import Image from "next/image";

import { navRefGlobal } from "@/components/modules/Nav";
import useClickOutside from "@/hooks/useClickOutside";
import useWindowResize from "@/hooks/useWindowResize";

import s from "./Header.module.scss";

const Header: React.FunctionComponent = () => {
  const { isMobile } = useWindowResize();

  useClickOutside(navRefGlobal, () => {
    if (navRefGlobal?.current && isMobile) {
      navRefGlobal.current.style.display = "none";
    }
  });

  return (
    <header className={s.header}>
      <div className="container">
        <div
          className={cn(
            "d-flex align-items-center justify-content-between",
            s.wrapper
          )}
        >
          <Image
            onClick={() => {
              const nav = navRefGlobal?.current;
              if (nav) {
                if (window.getComputedStyle(nav).display === "none") {
                  nav.style.display = "flex";
                } else {
                  nav.style.display = "none";
                }
              }
            }}
            className={s.hamburgerMenu}
            src="/images/hamburger-menu.png"
            alt="menu"
            width="39"
            height="36"
          />
          <Link href="/">
            <Image src="/images/icon.png" alt="icon" width="167" height="48" />
          </Link>
          <div className={s.textBlock}>
            <Image
              src="/images/secure.png"
              alt="secure"
              width="23"
              height="25"
            />
            <div>
              Cam kết an toàn trên mỗi giao dịch với ghi chú buôn bán tiền ảo
              hợp pháp.
            </div>
          </div>
          <div className={s.btnBlock}>
            <button className={cn(s.btnLogin, s.btn)}>ĐĂNG NHẬP</button>
            <button className={cn(s.btn, s.join)}>THAM GIA NGAY</button>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
