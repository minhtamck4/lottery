import React from "react";
import cn from "classnames";
import Image from "next/image";

import s from "./Footer.module.scss";

const Footer: React.FunctionComponent = () => {
  return (
    <footer className={s.footer}>
      <div className="container d-flex flex-column justify-content-between">
        <div className="row">
          <div className={cn("col-md-4 d-flex flex-column justify-content-between", s.top)}>
            <a href="#">
              <Image
                src="/images/logo-footer.png"
                alt="icon"
                width="92"
                height="103"
              />
            </a>
            <div className={s.social}>
              <a href="#">
                <Image
                  src="/images/facebook.png"
                  alt="icon"
                  width="37"
                  height="36"
                />
              </a>
              <a href="#">
                <Image
                  src="/images/telegram.png"
                  alt="icon"
                  width="37"
                  height="36"
                />
              </a>
              <a href="#">
                <Image
                  src="/images/zalo.png"
                  alt="icon"
                  width="37"
                  height="36"
                />
              </a>
            </div>
          </div>
          <div className={cn("col-md-8", s.middle)}>
            <h6>Về Lode88</h6>
            <p>
              Lode88 tự hào là nhà cái cung cấp cách dịch vụ lô đề, số đề, soi
              cầu lô đề hàng đầu tại Việt Nam. Lode88 đã đặt bước đi tiên phong
              trong lĩnh vực Đánh đề trực tuyến, hội đủ....
            </p>
            <div className={s.middle}>
              <h6>trách nhiệm</h6>
              <p>
                Lode88 tự hào là nhà cái cung cấp cách dịch vụ lô đề, số đề, soi
                cầu lô đề hàng đầu tại Việt Nam. Lode88 đã đặt bước đi tiên
                phong trong lĩnh vực Đánh đề trực tuyến, hội đủ cách kiểu đánh
                lô đề .....
              </p>
            </div>
            <h6>trợ giúp</h6>
            <div className="d-flex justify-content-between">
              <div>
                <p>
                  <span className={s.circle}></span> Khuyến mại
                </p>
                <p>
                  <span className={s.circle}></span> Hướng dẫn đánh đề
                </p>
              </div>
              <div>
                <p>
                  <span className={s.circle}></span>Kinh nghiệm đánh lô, số đề
                </p>
                <p>
                  <span className={s.circle}></span> Soi cầu
                </p>
              </div>
              <div>
                <p>
                  <span className={s.circle}></span>Giải mã giấc mơ
                </p>
                <p>
                  <span className={s.circle}></span>Mẹo cá độ
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className={cn("row text-center", s.bottom)}>
          <div className={s.footer_copyright}>
            Lô đề online - Đánh đề trực tuyến - Đánh lô số đề trên mạng - Chơi
            lô đề - Soi cầu lô đề - Giải mã giấc mơ - Kinh nghiệm đánh lô đề, số
            đề
          </div>
          <div className={cn(s.footer_copyright, s.mt7)}>
            ©{new Date().getFullYear()} LODE88 All rights resered.
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
