import React, { useState } from "react";
import Form from "react-bootstrap/Form";

const CustomMenu = React.forwardRef(
  (
    { children, style, className, "aria-labelledby": labeledBy }: any,
    ref: any
  ) => {
    const [value, setValue] = useState("");
    return (
      <div
        ref={ref}
        style={style}
        className={className}
        aria-labelledby={labeledBy}
      >
        <Form.Control
          autoFocus
          className="mx-3 my-2 w-auto"
          placeholder="Nhập kênh"
          onChange={e => setValue(e.target.value)}
          value={value}
        />
        <div>
          {React.Children.toArray(children).filter(
            (child: any) =>
              !value ||
              child?.props?.children?.toLowerCase?.()?.includes?.(value)
          )}
        </div>
      </div>
    );
  }
);

CustomMenu.displayName = "CustomMenu";
export default CustomMenu;
