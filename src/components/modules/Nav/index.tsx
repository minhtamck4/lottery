import React, { useEffect, useRef } from "react";
import Link from "next/link";
import cn from "classnames";

import s from "./Nav.module.scss";

const MENU_LINKS = [
  {
    id: 0,
    text: "MIỀN BẮC",
    url: "#",
  },
  {
    id: 1,
    text: "MIỀN TRUNG",
    url: "#",
  },
  {
    id: 2,
    text: "MIỀN NAM",
    url: "#",
  },
  {
    id: 3,
    text: "LÔ ĐỀ SIÊU TỐC",
    url: "#",
  },
  {
    id: 4,
    text: "THỂ THAO",
    url: "#",
  },
  {
    id: 5,
    text: "QUAY SỐ",
    url: "#",
  },
  {
    id: 6,
    text: "MIỀN BẮC",
    url: "#",
  },
  {
    id: 7,
    text: "CỔNG GAME",
    url: "#",
  },
  {
    id: 8,
    text: "SÒNG BÀI",
    url: "#",
  },
  {
    id: 9,
    text: "KENO VIETLOTT",
    url: "#",
  },
  {
    id: 10,
    text: "KENO 24/7",
    url: "#",
  },
  {
    id: 11,
    text: "NUMBERS GAME",
    url: "#",
  },
  {
    id: 12,
    text: "SỰ KIỆN",
    url: "#",
  },
];

export let navRefGlobal = null as any;

const Nav: React.FunctionComponent = () => {
  const navRef = useRef(null);
  useEffect(() => {
    navRefGlobal = navRef;
  }, [navRef]);

  return (
    <nav className={s.nav}>
      <div className="container">
        <div id="nav-list" ref={navRef} className={s.itemBlock}>
          {MENU_LINKS?.map(menu => (
            <div className={s.item} key={menu.id}>
              <Link href={menu.url}>{menu.text}</Link>
              <span className={s.line} />
            </div>
          ))}
        </div>
      </div>
    </nav>
  );
};

export default Nav;
