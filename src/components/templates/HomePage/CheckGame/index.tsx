import React, { useEffect, useState } from "react";
import Image from "next/image";
import ToggleButton from "react-bootstrap/ToggleButton";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";
import DatePicker from "react-datepicker";
import cn from "classnames";
import { format } from "date-fns";

import { SERVER_ID } from "@/config/common";
import "react-datepicker/dist/react-datepicker.css";
import { getLotteryPrize, getLotteryChannel } from "@/services/api/common";
import CustomMenu from "@/components/modules/CustomMenu";
import Empty from "@/components/modules/Empty";

import s from "./CheckGame.module.scss";

const RADIOS = [
  { name: "Dò thường", value: "1" },
  { name: "Dò 2 số", value: "2" },
  { name: "Dò 3 số", value: "3" },
];

const CheckGame: React.FunctionComponent = () => {
  const [radioValue, setRadioValue] = useState<string>("1");
  const [dateValue, setDateValue] = useState<Date>(new Date());
  const [dataFilter, setDataFilter] = useState<any>();
  const [dataSelecting, setDataSelecting] = useState<any>();
  const [prize, setPrize] = useState<any>();

  const fetchData = async () => {
    const filter = await getLotteryChannel();
    if (filter) {
      setDataFilter(filter);
      setDataSelecting(filter?.[0]?.subLotteryInfo?.[0]);
    }
  };

  const getPrize = async () => {
    if (dateValue && dataSelecting) {
      const result = await getLotteryPrize({
        serverId: SERVER_ID,
        channelId: dataSelecting?.id,
        rangDate: `${format(dateValue, "dd-MM-yyyy")}`,
        size: 1,
      });
      setPrize({
        ...result,
        gameResult: {
          numbersResults: result?.gameResult?.numbersResults?.reverse(),
        },
      });
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    if (dataSelecting && dateValue) {
      getPrize();
    }
  }, [dataSelecting, dateValue]);

  return (
    <div className={s.checkGame}>
      <div className={s.table}>
        <div className={s.header}>
          <div className={s.control}>
            <div>
              <form action="">
                <input
                  className={s.borderInput}
                  id="findNumber"
                  name="findNumber"
                  type="number"
                  placeholder="NHẬP SỐ"
                />
              </form>
            </div>
            <div>
              <DatePicker
                className={s.borderInput}
                selected={dateValue}
                onChange={date => setDateValue(date || new Date())}
              />
            </div>
            <div>
              <Dropdown className={cn(s.borderInput, s.menu)}>
                <Dropdown.Toggle id="dropdown-basic">
                  {dataSelecting?.name || "MIỀN BẮC"}
                </Dropdown.Toggle>
                <Dropdown.Menu as={CustomMenu}>
                  {dataFilter?.map((groupItem: any) => (
                    <>
                      <Dropdown.Item
                        key={groupItem?.groupId}
                        className={s.parent}
                      >
                        {groupItem?.groupName}
                      </Dropdown.Item>
                      <>
                        {groupItem?.subLotteryInfo?.map((item: any) => (
                          <Dropdown.Item
                            key={item?.id}
                            className={s.children}
                            onClick={() => setDataSelecting(item)}
                          >
                            {item?.name}
                          </Dropdown.Item>
                        ))}
                      </>
                    </>
                  ))}
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
          <div className={s.searchBtn}>
            <button className={s.btn}>XEM</button>
          </div>
        </div>
        <div className={s.body}>
          <div className={cn(s.item, s.grayBackground)}>
            <div
              className={s.heightLineText}
            >{`KẾT QUẢ XỔ SỐ ĐÀI ${dataSelecting?.name?.toUpperCase()}`}</div>
            <div className={s.heightLineText}>
              {format(dateValue, "eeee dd.MM.yyyy")}
            </div>
          </div>
          <div className={s.item}>
            <ButtonGroup className={s.radioBlock}>
              {RADIOS.map((radio, idx) => (
                <ToggleButton
                  key={idx}
                  id={`radio-${idx}`}
                  type="radio"
                  name="radio"
                  value={radio.value}
                  checked={radioValue === radio.value}
                  onChange={e => setRadioValue(e.currentTarget.value)}
                >
                  {radio.name}
                </ToggleButton>
              ))}
            </ButtonGroup>
          </div>
          {prize?.gameResult?.numbersResults &&
          prize?.gameResult?.numbersResults?.length > 0 ? (
            <>
              {prize?.gameResult?.numbersResults?.map(
                (item: any, index: number) => (
                  <div
                    className={cn(s.item, index % 2 === 0 && s.grayBackground)}
                    key={item?.type}
                  >
                    <div className={cn(item?.type === "DB" && s.specialPrize)}>
                      {item?.type}
                    </div>
                    <div className={cn(item?.type === "DB" && "w-40")}>
                      <div
                        className={cn(item?.type === "DB" && s.specialPrize)}
                      >
                        {item.numbers.join(", ")}
                      </div>
                    </div>
                  </div>
                )
              )}
            </>
          ) : (
            <Empty className={s.noData} />
          )}

          {/* <div className={cn(s.item, s.grayBackground)}>
            <div>Giải bảy</div>
            <div>
              <div>49 45 21 </div>
            </div>
          </div>
          <div className={s.item}>
            <div>Giải Sáu</div>
            <div>
              <div>449 455 261</div>
            </div>
          </div>
          <div className={cn(s.item, s.grayBackground)}>
            <div>Giải Năm</div>
            <div>
              <div>4279 4875 2091 4875 2091 4875 2091</div>
            </div>
          </div>
          <div className={s.item}>
            <div>Giải Tư</div>
            <div>
              <div>4875 2091 4875 2091</div>
            </div>
          </div>
          <div className={cn(s.item, s.grayBackground)}>
            <div>Giải Ba</div>
            <div>
              <div>43875 20791 20791 20791 </div>
            </div>
          </div>
          <div className={s.item}>
            <div>Giải Nhì</div>
            <div>
              <div>43875 20791 20791 20791 43875 20791</div>
            </div>
          </div>
          <div className={cn(s.item, s.grayBackground)}>
            <div>Giải Nhất</div>
            <div>
              <div>63575 245474</div>
            </div>
          </div>
          <div className={s.item}>
            <div className={s.specialPrize}>Giải Đặc biệt</div>
            <div className="w-40">
              <div className={s.specialPrize}>87323</div>
            </div>
          </div> */}
        </div>
      </div>
      <div className={s.telegram}>
        <Image
          src="/images/telegram-block.webp"
          width="60"
          height="60"
          alt="telegram"
        />
      </div>
    </div>
  );
};

export default CheckGame;
