import React, { useEffect, useState } from "react";
import Countdown from "react-countdown";

import { format } from "date-fns";
import Empty from "@/components/modules/Empty";
import { getLotteryChannel } from "@/services/api/common";
import { CURRENCY_SYMBOL } from "@/config/common";

import s from "./BetOnline.module.scss";

const CHANNEL_IMG = [
  "url(/images/mb.png)",
  "url(/images/mt.png)",
  "url(/images/mn.png)",
];

const BetOnline: React.FunctionComponent = () => {
  const [channels, setChannels] = useState<Array<any> | null>();

  const fetchData = async () => {
    const result = await getLotteryChannel();
    setChannels(result);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const rendererCountDown = ({
    hours,
    minutes,
    seconds,
    completed,
  }: {
    hours: number;
    minutes: number;
    seconds: number;
    completed: boolean;
  }) => {
    if (completed) {
      // Render a complete state
      return <div className={s.doneText}>Chốt xổ</div>;
    } else {
      // Render a countdown
      return (
        <div className={s.time}>
          {hours}:{minutes}:{seconds}
        </div>
      );
    }
  };

  const resultTime = (date: number) => {
    return `${format(new Date(date), "dd-MM-yyyy")}`;
  };

  return (
    <div className={s.containerOnline}>
      {channels === null ? (
        <div>Loading...</div>
      ) : (
        <>
          {channels && channels?.length < 1 ? (
            <Empty />
          ) : (
            <>
              {channels?.map((channel, index) => (
                <div
                  className={s.itemOnline}
                  style={{ backgroundImage: CHANNEL_IMG?.[index] }}
                  key={`${index}-${channel?.resultTime}`}
                >
                  <div className={s.topOline}>
                    <div className={s.text}>Tổng cược trong ngày</div>
                    <div className={s.money}>
                      {channel?.totalBet} {CURRENCY_SYMBOL}
                    </div>
                  </div>
                  <div className={s.bottomOline}>
                    <Countdown
                      className={s.time}
                      date={
                        new Date(channel?.resultTime).getTime() -
                        new Date()?.getTime()
                      }
                      renderer={rendererCountDown}
                    />

                    <div className={s.date}>
                      {resultTime(channel?.resultTime)}
                    </div>
                    <div className={s.btn}>
                      <a href={channel?.url} target="_blank">{channel?.groupName}</a>
                    </div>
                  </div>
                </div>
              ))}
            </>
          )}
        </>
      )}
    </div>
  );
};

export default BetOnline;
