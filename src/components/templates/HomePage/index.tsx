import React from "react";
import Carousel from "react-bootstrap/Carousel";
// import cn from "classnames";

import Heading from "./Heading";
import BetOnline from "./BetOnline";
import Guid from "./Guid";
import Game from "./Game";
import Result from "./Result";
import CheckGame from "./CheckGame";
import s from "./HomePage.module.scss";

const HomePage: React.FunctionComponent = () => {
  return (
    <div className={s.home}>
      <Carousel>
        <Carousel.Item>
          <img className="img-fluid" src="/images/banner.png" alt="banner" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="img-fluid" src="/images/banner.png" alt="banner" />
        </Carousel.Item>
      </Carousel>

      <div className="container">
        <Heading
          tittle="Đánh đề trực tuyến"
          content="Chúng tôi là nhà cái uy tín nhất, với nguồn vốn từ Macau. Tỉ lệ trả thưởng cực cao, rút & nạp tiền nhanh, an toàn."
          imgSrc="/images/ticket.png"
          alt="ticket"
        />
        <BetOnline />
        <Heading
          tittle="Dò số bàn đề"
          content="Dò số nhanh, chính xác và cùng tán gẫu cùng các thánh đề khác"
          imgSrc="/images/lotto.png"
          alt="lotto"
        />
        <CheckGame />
        <Heading
          tittle="chơi game trung lớn"
          content="Các game được hàng triệu người chơi mỗi ngày trên hệ thống của chúng tôi"
          imgSrc="/images/dice.png"
          alt="dice"
        />
        <Game />
        <Result />
        <Guid />
      </div>
    </div>
  );
};

export default HomePage;
