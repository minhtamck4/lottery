import React from "react";
import Image from "next/image";

import s from "./Game.module.scss";

const Game: React.FunctionComponent = () => {
  return (
    <div className={s.gameWrapper}>
      <div className={s.seeMore}>
        <div className={s.text}>{`Xem thêm >>`}</div>
      </div>
      <div className={s.game}>
        <div className={s.item}>
          <a href="#">
            <Image
              width={159}
              height={159}
              src="/images/than-quay.png"
              alt="than quay"
            />
            <div className={s.title}>Thần Quay Mini</div>
            <div className={s.subText}>TQ247</div>
            <div className={s.reward}>$ 41.653.876</div>
          </a>
        </div>
        <div className={s.item}>
          <a href="#">
            <Image
              width={159}
              height={159}
              src="/images/mini-bar.png"
              alt="mini bar"
            />
            <div className={s.title}>Minibar</div>
            <div className={s.subText}>TQ247</div>
            <div className={s.reward}>$ 1.653.876</div>
          </a>
        </div>
        <div className={s.item}>
          <a href="#">
            <Image
              width={159}
              height={159}
              src="/images/mau-binh.png"
              alt="mau binh"
            />
            <div className={s.title}>Mậu Binh</div>
            <div className={s.subText}>TQ247</div>
            <div className={s.reward}>$ 1.653.876</div>
          </a>
        </div>
        <div className={s.item}>
          <a href="#">
            <Image
              width={159}
              height={159}
              src="/images/lucky-wild.png"
              alt="lucky-wild"
            />
            <div className={s.title}>Lucky Wild</div>
            <div className={s.subText}>TQ247</div>
            <div className={s.reward}>$ 1.653.876</div>
          </a>
        </div>
        <div className={s.item}>
          <a href="#">
            <Image
              width={159}
              height={159}
              src="/images/mini-poker.png"
              alt="lucky-wild"
            />
            <div className={s.title}>Mini Poker</div>
            <div className={s.subText}>TQ247</div>
            <div className={s.reward}>$ 1.653.876</div>
          </a>
        </div>
        <div className={s.item}>
          <a href="#">
            <Image width={159} height={159} src="/images/lode.png" alt="lode" />
            <div className={s.title}>Lô đề</div>
            <div className={s.subText}>TQ247</div>
            <div className={s.reward}>$ 1.653.876</div>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Game;
