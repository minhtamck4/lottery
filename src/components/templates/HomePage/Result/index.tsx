import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import cn from "classnames";

import { getPlayerBigWin } from "@/services/api/common";
import Empty from "@/components/modules/Empty";
import { numberWithCommas } from "@/utils/common";
import { CURRENCY_SYMBOL } from "@/config/common";

import s from "./Result.module.scss";

const Result: React.FunctionComponent = () => {
  const [players, setPlayers] = useState<Array<any> | null>();
  const listWinnerRef = useRef<HTMLDivElement>(null);

  const fetchData = async () => {
    const result = await getPlayerBigWin();
    setPlayers(result);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleScrollXResult = () => {
    if (listWinnerRef?.current) {
      listWinnerRef.current.scrollLeft += 170*2;
    }
  };

  return (
    <div className={s.result}>
      <div className={s.firstItem}>
        <Image
          src="/images/result-win.png"
          alt="icon"
          width="227"
          height="132"
        />
        <div className={s.text}>Người chiến thắng</div>
      </div>
      <div className={s.playersWin} ref={listWinnerRef}>
        {players === null ? (
          <div>Loading...</div>
        ) : (
          <>
            {players && players?.length < 1 ? (
              <Empty />
            ) : (
              <>
                {players?.map((player, index) => {
                  // is lasted ?
                  if (index === players?.length - 1) {
                    return null;
                  }
                  return (
                    <>
                      <div className={s.item} key={`${index}-${player?.name}`}>
                        <div className={s.title}>{player?.name}</div>
                        <div className={s.description}>Vừa chiến thắng</div>
                        <div className={s.reward}>
                          {numberWithCommas(player?.win)} {CURRENCY_SYMBOL}
                        </div>
                      </div>
                    </>
                  );
                })}
              </>
            )}
          </>
        )}
      </div>
      <div className={cn(s.item, s.lastedItem)}>
        <div className={s.title}>{players?.pop()?.name}</div>
        <div className={s.description}>Vừa chiến thắng</div>
        <div className={s.reward}>
          {numberWithCommas(players?.pop()?.win)} {CURRENCY_SYMBOL}
        </div>
        <div className={s.imgArrowRight} onClick={handleScrollXResult}>
          <Image
            src="/images/arrow-right.png"
            alt="icon"
            width="27"
            height="91"
          />
        </div>
      </div>
    </div>
  );
};

export default Result;
