import React from "react";
import cn from "classnames";
import Image from "next/image";

import s from "./Heading.module.scss";

const Heading: React.FunctionComponent<{
  tittle: string;
  content: string;
  imgSrc: string;
  alt: string;
}> = ({ tittle, content, imgSrc, alt }) => {
  return (
    <div className={cn(s.contentTicket, "d-flex flex-row ")}>
      <div className={s.ticket}>
        <Image width={57} height={57} src={imgSrc} alt={alt} />
      </div>
      <div>
        <div className={cn(s.tittle, "text-uppercase")}>{tittle}</div>
        <div className={s.textContent}>{content}</div>
      </div>
    </div>
  );
};

export default Heading;
