import React from "react";
import Image from "next/image";

import s from "./Guid.module.scss";

const Guid: React.FunctionComponent = () => {
  return (
    <>
      <div className={s.guid}>
        <div className={s.item}>
          <Image width={88} height={88} src="/images/symbol.png" alt="symbol" />
          <div className={s.subText}>Hướng dẫn</div>
          <div className={s.title}>KHUYẾN MÃI</div>
        </div>
        <div className={s.item}>
          <Image
            width={88}
            height={88}
            src="/images/money-withdrawal.png"
            alt="symbol"
          />
          <div className={s.subText}>Hướng dẫn</div>
          <div className={s.title}>NẠP TIỀN</div>
        </div>
        <div className={s.item}>
          <Image
            width={88}
            height={88}
            src="/images/cash-withdrawal.png"
            alt="symbol"
          />
          <div className={s.subText}>Hướng dẫn</div>
          <div className={s.title}>RÚT TIỀN</div>
        </div>
        <div className={s.item}>
          <Image width={88} height={88} src="/images/balls.png" alt="symbol" />
          <div className={s.subText}>Hướng dẫn</div>
          <div className={s.title}>ĐÁNH ĐỀ</div>
        </div>
      </div>
      <div className={s.imgBlock}>
        <img src="/images/friend.png" alt="friend" />
      </div>
    </>
  );
};

export default Guid;
