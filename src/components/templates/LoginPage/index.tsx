import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import Router from "next/router";
import cn from "classnames";

import s from "./styles.module.scss";

const LoginPage: React.FunctionComponent = () => {
  return (
    <div className={s.login}>
      <h1>Login</h1>
      <Formik
        initialValues={{ email: "", password: "" }}
        validate={values => {
          const errors = {} as any;
          if (!values.email) {
            errors.email = "Required";
          }
          if (!values.password) {
            errors.password = "Required";
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          document.cookie = `userName=${values.email?.trim()}`;
          document.cookie = `password=${values.password?.trim()}`;
          Router.push("/");
          setSubmitting(false);
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Field className="form-control" type="text" name="email" placeholder="user name" />
            <ErrorMessage className={s.error} name="email" component="div" />
            <Field
              className="mt-2 form-control"
              type="password"
              name="password"
              placeholder="password"
            />
            <ErrorMessage className={s.error} name="password" component="div" />
            <div className="d-flex justify-content-center">
              <button
                className={cn("mt-2", s.btn)}
                type="submit"
                disabled={isSubmitting}
              >
                Submit
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default LoginPage;
