import React from "react";

import Header from "@/components/modules/Header";
import Footer from "@/components/modules/Footer";
import Nav from "@/components/modules/Nav";

import s from "./Default.module.scss";

type Props = {
  children: React.ReactNode;
};

const Default: React.FunctionComponent<Props> = ({ children }: Props) => {
  return (
    <>
      <Header />
      <Nav />
      <main className={s.default}>{children}</main>
      <Footer />
    </>
  );
};

export default Default;
