import axios from "@/services/http";
// import { Post } from "@/interfaces/post";

const CATEGORY_URL = "history";

export const getPlayerBigWin = async (): Promise<any> => {
  try {
    const { data } = await axios.get(`${CATEGORY_URL}/player-big-win`);
    return data.data;
  } catch (error) {
    // log error
    return [];
  }
};

export const getLotteryChannel = async (): Promise<any> => {
  try {
    const { data } = await axios.get(`rest-game/lode/api/game/landing`);
    return data?.data?.lottery || [];
  } catch (error) {
    // log error
    return [];
  }
};

export const getLotteryPrize = async ({
  serverId,
  rangDate,
  channelId,
  size,
}: {
  serverId: number;
  rangDate: string;
  channelId: string;
  size: number;
}): Promise<any> => {
  try {
    const { data } = await axios.get(
      `history/10?sId=${serverId}&chId=${channelId}&rd=${rangDate}&sz=${size}`
    );
    return data?.items?.[0] || {};
  } catch (error) {
    // log error
    return {};
  }
};
