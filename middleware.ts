import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export const middleware = (request: NextRequest) => {
  const userName = request.cookies.get("userName");
  const password = request.cookies.get("password");
  if (userName && password) {
    if (userName?.value !== "root" || password?.value !== "raffle")
      return NextResponse.redirect(new URL("/login", request.url));
  } else {
    return NextResponse.redirect(new URL("/login", request.url));
  }
};

export const config = {
  matcher: "/",
};
