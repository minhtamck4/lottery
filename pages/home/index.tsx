import { GetServerSideProps, NextPage } from "next";

import HomePageTemplate from "@/components/templates/HomePage";
import { SeoInfo } from "@/interfaces/base";

type IProps = {
  seoInfo: SeoInfo;
};

const HomePage: NextPage<IProps> = () => {
  return <HomePageTemplate />;
};

export const getServerSideProps: GetServerSideProps<IProps> = async () => {
  return {
    props: {
      seoInfo: {
        title: "lottery",
        description:
          "description",
        seoImage: "seoImage",
        keywords: "lottery",
      },
    },
  };
};

export default HomePage;
