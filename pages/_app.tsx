import localFont from "next/font/local";
import type { AppProps } from "next/app";

import DefaultLayout from "@/components/layouts/Default";
import CustomHead from "@/components/layouts/CustomHead";

import "@/styles/index.scss";

const baiJamjuree = localFont({
  src: [
    {
      path: "../public/fonts/BaiJamjuree-Regular.ttf",
      weight: "400",
      style: "normal",
    },
    {
      path: "../public/fonts/BaiJamjuree-Medium.ttf",
      weight: "500",
      style: "normal",
    },
    {
      path: "../public/fonts/BaiJamjuree-SemiBold.ttf",
      weight: "600",
      style: "normal",
    },
    {
      path: "../public/fonts/BaiJamjuree-Bold.ttf",
      weight: "700",
      style: "normal",
    },
  ],
});

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <style jsx global>{`
        html {
          font-family: ${baiJamjuree.style.fontFamily} !important;
        }
      `}</style>
      <CustomHead seoInfo={pageProps?.seoInfo} />
      {pageProps.noLayout ? (
        <Component {...pageProps} />
      ) : (
        <DefaultLayout>
          <Component {...pageProps} />
        </DefaultLayout>
      )}
    </>
  );
}

export default App;
