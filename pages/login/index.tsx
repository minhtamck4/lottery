import { GetServerSideProps, NextPage } from "next";

import LoginPageTemplate from "@/components/templates/LoginPage";
import { SeoInfo } from "@/interfaces/base";

type IProps = {
  seoInfo: SeoInfo;
};

const HomePage: NextPage<IProps> = () => {
  return <LoginPageTemplate />;
};

export const getServerSideProps: GetServerSideProps<IProps> = async () => {
  return {
    props: {
      noLayout: true,
      seoInfo: {
        title: "lottery",
        description: "description",
        seoImage: "seoImage",
        keywords: "lottery",
      },
    },
  };
};

export default HomePage;
